package controler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import sample.Quiz;

public class StartController implements EventHandler<ActionEvent> {
	
	Quiz quiz;
	
	@FXML
    private Button b_startQuiz, b_drzave;

    @FXML
    private void initialize() {
    	b_startQuiz.setOnAction(this);
    	b_drzave.setOnAction(this);
    }

	@Override
	public void handle(ActionEvent event) {

        Quiz.startQuiz();
	}
	
}
