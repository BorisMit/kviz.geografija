package controler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.Drzava;
import service.LoadDrzave;

public class DrzavaController implements EventHandler<ActionEvent> {

    @FXML
    private TextField tf_naziv, tf_grad, tf_broj, tf_povrsina, tf_kontinent, tf_dohodak;

    @FXML
    private Button b_snimi;

    public DrzavaController(TextField tf_naziv, TextField tf_grad, TextField tf_broj, TextField tf_povrsina, TextField tf_kontinent, TextField tf_dohodak, Button b_snimi) {
        this.tf_naziv = tf_naziv;
        this.tf_grad = tf_grad;
        this.tf_broj = tf_broj;
        this.tf_povrsina = tf_povrsina;
        this.tf_kontinent = tf_kontinent;
        this.tf_dohodak = tf_dohodak;
        this.b_snimi = b_snimi;
    }

    @FXML
    private void initialize() {
        b_snimi.setOnAction(this);
    }

    @Override
    public void handle(ActionEvent e) {
        Object source = e.getSource();

        if (source instanceof Button) {
            ucitajDrzavu();
        }
    }

    private void ucitajDrzavu() {
        String naziv = tf_naziv.getText().trim();
        String glavniGrad = tf_grad.getText().trim();
        String brojStanovnika = tf_broj.getText().trim();
        String povrsina = tf_povrsina.getText().trim();
        String kontinent = tf_kontinent.getText().trim();
        String dohodak = tf_dohodak.getText();

        Drzava drzava = new Drzava(naziv, glavniGrad, Long.valueOf(brojStanovnika), Long.valueOf(povrsina), kontinent, Long.valueOf(dohodak));
        LoadDrzave load = new LoadDrzave();
        load.unesiDrzave(drzava);
    }
}
