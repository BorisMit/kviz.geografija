package model;

public class Drzava {
	
	private String naziv;
	private String glavniGrad;
	private long brojStanovnika;
	private long povrsina;
	private String kontinent;
	private long dohodak;
	private int gustinaNaseljenosti;
	
	public Drzava(String naziv, String glavniGrad, long brojStanovnika, long povrsina, String kontinent, long dohodak) {
		super();
		this.naziv = naziv;
		this.glavniGrad = glavniGrad;
		this.brojStanovnika = brojStanovnika;
		this.povrsina = povrsina;
		this.kontinent = kontinent;
		this.dohodak = dohodak;
		this.gustinaNaseljenosti = odrediGustinu(brojStanovnika, povrsina);
	}
	
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getGlavniGrad() {
		return glavniGrad;
	}
	public void setGlavniGrad(String glavniGrad) {
		this.glavniGrad = glavniGrad;
	}
	public long getBrojStanovnika() {
		return brojStanovnika;
	}
	public void setBrojStanovnika(long brojStanovnika) {
		this.brojStanovnika = brojStanovnika;
	}
	public long getPovrsina() {
		return povrsina;
	}
	public void setPovrsina(long povrsina) {
		this.povrsina = povrsina;
	}
	public String getKontinent() {
		return kontinent;
	}
	public void setKontinent(String kontinent) {
		this.kontinent = kontinent;
	}
	public long getDohodak() {
		return dohodak;
	}
	public void setDohodak(long dohodak) {
		this.dohodak = dohodak;
	}
	
	public int getGustinaNaseljenosti() {
		return gustinaNaseljenosti;
	}

	private static int odrediGustinu(long brojStanovnika, long povrsina){
		return (int) ((int) brojStanovnika/povrsina);
	}

}
