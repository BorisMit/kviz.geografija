package model;

import service.CSVReader;

import java.util.*;

public class Pitanja {

    public String formirajPitanje(){

        Map<Integer, Drzava> mapKviz = mapaKviz();
        List<Integer> brojeviDrzava = randomArray(20, mapKviz.size());

        return "";
    }

    public Map<Integer, Drzava> mapaKviz(){
        List<Drzava> drzTemp = CSVReader.ucitajDrzave();
        Map<Integer, Drzava> mapaDrzava = new HashMap<Integer, Drzava>();
        int count = 0;
        for (Drzava drzava : drzTemp) {
            count ++;
            mapaDrzava.put(count, drzava);
        }

        return mapaDrzava;
    }

    public ArrayList<Integer> randomArray(int brojPitanja, int brojDrzava){
        Random random = new Random();

        ArrayList<Integer> newArrayList = new ArrayList<Integer>();

        for(int i=0; i<brojPitanja; i++){
            Integer next = random.nextInt(brojDrzava) + 1;
            newArrayList.add(next);
        }
        return newArrayList;
    }

    public int izracunajRandom(int broj){

        Random rand = new Random();
        return rand.nextInt(broj) + 1;

    }
}
