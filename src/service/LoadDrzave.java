package service;

import model.Drzava;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoadDrzave {

    public void unesiDrzave(Drzava drzava) {
        try {
            boolean prviUnos = false;
            List<Drzava> ucitaneDrzave = CSVReader.ucitajDrzave();
            if (ucitaneDrzave.isEmpty()) {
                prviUnos = true;
                proveriPodatke(prviUnos, drzava);
            } else {
                proveriPodatke(prviUnos, drzava);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void proveriPodatke(boolean prviUnos, Drzava drzava) throws IOException {

        String csvFile = "C:/work/drzave.csv";
        FileWriter writer;
        if (prviUnos) {
            writer = new FileWriter(csvFile);
        } else {
            writer = new FileWriter(csvFile, true);
        }


        List<Drzava> drzave = new ArrayList<Drzava>();
        drzave.add(drzava);

        //for header
        if (prviUnos) {
            CSVUtils.writeLine(writer, Arrays.asList("Naziv", "GlavniGrad", "BrojStanovnika", "Povrsina", "Kontinent", "Dohodak", "GustinaNaseljenosti"));
        }

        for (Drzava d : drzave) {

            List<String> list = new ArrayList<>();
            list.add(d.getNaziv());
            list.add(d.getGlavniGrad());
            list.add(String.valueOf(d.getBrojStanovnika()));
            list.add(String.valueOf(d.getPovrsina()));
            list.add(d.getKontinent());
            list.add(String.valueOf(d.getDohodak()));
            list.add(String.valueOf(d.getGustinaNaseljenosti()));

            CSVUtils.writeLine(writer, list);

            //try custom separator and quote.
            //CSVUtils.writeLine(writer, list, '|', '\"');
        }

        writer.flush();
        writer.close();

    }
}
