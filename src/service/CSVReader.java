package service;

import model.Drzava;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CSVReader {

    public static List<Drzava> ucitajDrzave() {

        String csvFile = "C:/work/drzave.csv";
        String cvsSplitBy = ",";
        List<Drzava> listaDrzava = new ArrayList<Drzava>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            String line = "";

            if (br.readLine() != null) {
                while ((line = br.readLine()) != null) {

                    // use comma as separator
                    String[] country = line.split(cvsSplitBy);

                    String naziv = country[0];
                    String glavniGrad = country[1];
                    String brojStanovnika = country[2];
                    String povrsina = country[3];
                    String kontinent = country[4];
                    String dohodak = country[5];

                    Drzava drzava = new Drzava(naziv, glavniGrad, Long.valueOf(brojStanovnika), Long.valueOf(povrsina), kontinent, Long.valueOf(dohodak));

                    listaDrzava.add(drzava);

                }
            } else {
                listaDrzava = Collections.emptyList();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return listaDrzava;

    }

}